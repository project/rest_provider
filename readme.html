<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
  <head>
    <title>REST Provider Documentation</title>
    <style type="text/css">
      body {
        font: 13px/160% Verdana, sans-serif;
      }
      h1, h2, h3, h4 {
        font-family: Georgia, serif;
      }
      pre {
        background: #eee;
        line-height: 130%;
        padding: 1em;
      }
      code {
        background: #eee;
      }
      .assumption {
        font-style: italic;
      }
    </style>
  </head>
  <body>
    <h1>The REST API</h1>

    <h2>Abstract</h2>
    <p>The REST Provider module provides a simple framework for creating RESTful web services using Drupal. It strives to be simple and unobtrusive, imposing as few constraints on developers as possible. Developers are free to create any kind of RESTful web service, not just "Drupalesque" services. This module also takes care of some of the more tedious aspects of creating a RESTful web service.</p>

    <h2>Difference From Other REST Modules</h2>
    <p>There are currently three other REST-related modules for Drupal. Here's how this module compares to those:</p>
    <ul>
      <li><a href="http://drupal.org/project/restapi">REST API</a> &mdash; This module's purpose is to access Drupal functions through a (mostly) RESTful interface, not creating web services with Drupal.</li>
      <li><a href="http://drupal.org/project/rest">RESTfulness</a> &mdash; This module's purpose, like the REST API module, is to access Drupal functions through a RESTful interface. The module has had no releases and doesn't appear to be in active development.</li>
      <li><a href="http://drupal.org/project/rest_server">REST Server</a> &mdash; This module's purpose is to provide a (somewhat) RESTful implementation of the Services API. It is thus beholden to the Services module's constraints.</li>
    </ul>

    <h2>Architecture</h2>
    <p>At its heart, the REST Provider module provides a simple controller for automatically invoking the proper function for the given request (based on its URI and HTTP request method).</p>
    <p>The REST Provider module relies heavily on hooks and conventions. All request handlers are hook functions and all controllers are represented with <code class="filename">.inc</code> files that contain their respective handler functions. By adhering to such a structure, RESTful web services are easy to create, understand, and maintain.</p>
    <p>This is a standalone module. It doesn't use the <a href="http://drupal.org/project/services">Services module</a>'s API because the Services module treats REST as just another web services architecture. The basic REST model is unique enough that tools for implementing it should be designed specifically for that purpose.</p>
    <p>This module is lightweight. It uses as few resources as possible. There are no database queries in the module itself (although the module does call <code>user_access()</code>, which queries the database the first time it's run). The module accesses one additional file per request.</p>

    <h2>Simplifying Assumptions</h2>
    <p>The REST Provider module imposes a few simplifying assumptions on services. They are:</p>
    <ol>
      <li><span class="assumption">The service will be on the web.</span> Drupal's natural home is the web and this module assumes that's where the service will be. And though REST, as described by <a href="http://www.ics.uci.edu/~fielding/">Roy T. Fielding</a>, is not limited to the web or HTTP, this module only works in this context.</li>
      <li><span class="assumption">OPTIONS requests should be handled automatically.</span> The response to HTTP OPTIONS requests is very simple: tell the user what HTTP methods are available on the requested resource. This module performs reflection on the service to determine which HTTP methods are available to each resource and automatically sends a response to OPTIONS requests. This requires no additional work on the part of service developers.</li>
      <li><span class="assumption">Authentication has already been handled.</span> This module provides no authenication mechanism, nor does it integrate with one. Authentication should be among the first things handled in the HTTP request. By the time the request finds its way to the REST API controller, the request should have already been authenticated.</li>
      <li><span class="assumption">Requests are valid.</span> This module does not prevent <a href="http://en.wikipedia.org/wiki/Cross-site_request_forgery">CSRF</a>s or any other types of attacks by checking to see if a request is valid or if the authentication is valid. It assumes that these checks have already taken place and the request refused before it reaches the REST API controller.</li>
      <li><span class="assumption">Proxies are not a concern.</span> This module doesn't handle CONNECT or TRACE requests because they deal with HTTP proxies, which are outside of the scope of this module.</li>
    </ol>

    <h2>Using the API</h2>
    <p>Using the REST Provider module requires creating a custom module. This module will need to implement <code>hook_menu()</code> in order to invoke the controller. For each URI that the controller should handle, create a <code>MENU_CALLBACK</code> menu item.</p>
    <h3>Invoking the Controller</h3>
    <p>Let's assume that we're creating a module called <code>example</code> on a site hosted at <code>http://example.com/</code> and look in the <code>example.module</code> file:</p>
    <pre><code>function example_menu() {
  $items['myuri'] = array(
    'title' =&gt; 'Example URI',
    'type' =&gt; MENU_CALLBACK,
    'page callback' =&gt; 'rest_provider_controller',
    'page arguments' =&gt; array('example', 'mycontroller'),
    'access callback' =&gt; 'rest_provider_access',
  );
}</code></pre>
    <p>In this example, the URI <code>http://example.com/myuri</code> is told to use a REST Provider controller. This controller is contained within the <code>example</code> module (in this case, the same module) and the controller is called <code>mycontroller</code>. These are the aguments that are required to be sent to <code>rest_provider_controller()</code>.</p>
    <p>Because <code>rest_provider_controller()</code> allows you to specify a module name as well as a controller name, you can create a custom module to link controllers defined in other modules to paths that you define.</p>
    <p>Additionally, <code>rest_provider_controller()</code> supports wildcards in menu items. Say you want the URIs <code>http://example.com/myuri/abc/show</code> and <code>http://example.com/myuri/xyz/show</code> to be sent to the same controller with <code>abc</code> and <code>xyz</code> (respectively) sent as arguments to the controller. You might implement it like this:</p>
    <pre><code>function example_menu() {
  $items['myuri/%/show'] = array(
    'title' =&gt; 'Example URI',
    'type' =&gt; MENU_CALLBACK,
    'page callback' =&gt; 'rest_provider_controller',
    'page arguments' =&gt; array('example', 'mycontroller', 1),
    'access callback' =&gt; 'rest_provider_access',
  );
}</code></pre>
    <p>In this example, any other value between <code>myuri/</code> and <code>/list</code> is sent as an argument to <code>rest_provider_controller()</code>. You can send an arbitrary number of arguments to this function in this manner. Just tack them onto the end of the <code>page arguments</code> array.</p>
    <h3>Handling Requests</h3>
    <p>The example module has linked a URI&mdash;or, in the latter example, a URI template&mdash;to a REST Provider controller. Implementing the controller is as simple as creating a file called <code>example_mycontroller.inc</code> in the <code>example</code> module's directory. The directory structure might now look like this:</p>
    <pre><code>sites/all/modules/example/example.info
sites/all/modules/example/example.module
sites/all/modules/example/example_mycontroller.inc</code></pre>
    <p>It might be obvious from the example, the REST Provider module looks in a module's directory for a file called <code>MODULENAME_CONTROLLERNAME.inc</code>.</p>
    <p>Finally, handling the request is as simple as creating a function in the new controller's <code>.inc</code> file:</p>
    <pre><code>function example_mycontroller_GET($my_arg) {
  return array(
    'response_code' =&gt; '200', // OK
    'headers' =&gt; array(),
    'body' =&gt; $my_arg,
    'media_type' =&gt; 'text/plain',
    'charset' =&gt; 'utf-8',
  );
}</code></pre>
    <p>The function name follows a similar naming convention as the controller itself: <code>MODULENAME_CONTROLLERNAME_METHOD()</code> where <code>METHOD</code> is the name of an HTTP method. If you create a method called <code>example_mycontroller_POST</code>, all POST requests will automatically be sent to that function.</p>
    <p>You'll note in the example that the function accepts an argument called <code>$my_arg</code>. This is populated with the argument from the menu item: that is, <code>abc</code> or <code>xyz</code> in the example URIs. So loading the URI <code>http://example.com/myuri/abc/show</code> will result in <code>$my_arg</code> having the value <code>abc</code>.</p>
    <h3>Responses</h3>
    <p>If your handler function doesn't return a value, an HTTP/1.1 200 response (OK) will be sent along with an empty response body. But because the example function above specified a return value (that is, an associative array), the following HTTP response would be sent:</p>
    <pre><code>200 OK
Content-Type: text/plain; utf-8

abc</code></pre>
    <p>Well, that's not entirely true. Because the REST Provider module will automatically gzip responses if the option is enabled in Drupal and it's supported by the server, the request would look more like this:</p>
    <pre><code>200 OK
Content-Type: text/plain; utf-8
Content-Encoding: gzip

[binary gzipped representation of "abc"]</code></pre>
    <p>The gzip handling is completely automatic so you don't even need to think about it.</p>
    <h3>Permissions</h3>
    <p>All of this is well and good if you want every HTTP request to be open to every client. But most of the time, you're going to want to restrict access to most, if not all, of your exposed resources. There are two ways to accomplish this.</p>
    <p>First, in your menu items, you can use the default <code>user_access</code> function as your <code>access callback</code> function and specify a permission as your <code>access arguments</code>:</p>
    <pre><code>function example_menu() {
  $items['myuri/%/show'] = array(
    'title' =&gt; 'Example URI',
    'type' =&gt; MENU_CALLBACK,
    'page callback' =&gt; 'rest_provider_controller',
    'page arguments' =&gt; array('example', 'mycontroller', 1),
    'access callback' =&gt; 'user_access',
    'access arguments' =&gt; array('my permission'),
  );
}</code></pre>
    <p>But this can prove to be problematic: this permission will apply to <em>all</em> requests to that URI, regardless of the method used. Which means that, for example, GET, PUT, and DELETE requests would all share the same permissions. This is rarely desirable in a RESTful application.</p>
    <p>Thus the <code>rest_provider_access</code> function exists. Using it will allow all requests to proceed past the menu system without checking for permissions. This point should be reiterated: <strong>If you use <code>rest_provider_access</code>, the menu system will not check for permissions.</strong> Using this carelessly <strong>could be a major security risk</strong>.</p>
    <p>So if <code>rest_provider_access</code> could present a security risk, why use it? Because it will shift the responsibility of checking for permissions from the menu system to the REST Provider module, which gives you fine-grained control of permissions in each controller.</p>
    <p>Implementing permissions is, again, very simple. Just add a <code>hook_permissions()</code> function to the controller file. In the example above, this code would go in the <code>example_mycontroller.inc</code> file:</p>
    <pre><code>function example_mycontroller_permissions() {
  return array(
    'GET' =&gt; array('GET permission'),
    'PUT' =&gt; array('PUT permission', 'another permission'),
  );
}</code></pre>
    <p>When a GET request is sent to <code>http://example.com/myuri/abc/show</code>, the REST Provider module will check to see if the current user has the permission <code>GET permission</code>. If the user doesn't have said permission, the REST Provider module will return a 403 Forbidden or 401 Unauthorized (depending on whether a user has been authenticated) and the request will never even reach the <code>example_mycontroller_GET</code> function. Similarly, PUT requests would have to pass permission checks for <code>PUT permission</code> <strong>and</strong> <code>another permission</code> for the <code>example_mycontroller_PUT</code> function to be called.</p>
    <p>Please note: The default permission is <strong>allow</strong>. If you do not provide an entry in your <code>hook_permissions()</code> function for a corresponding method, users will not be denied access by the REST Provider module. This is by design. This allows you to use both the menu system's access check and the REST Provider's access check for the same request.</p>
  </body>
</html>